#pragma once

#include "ofMain.h"
#include "displaceMesh.h"
#include "fftHandler.h"
#include "ofxBlur.h"

class master : public ofBaseApp{
public:
    void setup();
    void update();
    void draw();
    
    void keyPressed(int key);
    void keyReleased(int key);
    void mouseMoved(int x, int y);
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);
    void audioIn(float *input, int bufferSize, int nChannels);
    void audioOut(float *output, int bufferSize, int nChannels);
    void exit();
    
    displaceMesh writer;
    displaceMesh writer2;
    fftHandler fft;
    ofxBlur blur;
    
    ofFbo whiter;
    
    int counter;
    int timer;
    vector <string> woorden;
    vector <string> woorden2;

};
