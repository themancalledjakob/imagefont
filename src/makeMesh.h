#pragma once

#include "ofMain.h"
#include "ofxDelaunay.h"

class makeMesh : public ofBaseApp{
    
public:
	void setup();
	void update(ofPath path, int texWrapMethod);
	void draw();
    
    void makeTextureCoords(ofMesh * mesh, int texWrapMethod);
	
	bool isPointInsidePolygon(ofPoint *polygon, int N, ofPoint p);
	bool isPointInsideOfShape(vector<ofVec3f> * path, int N, ofVec3f p);
	void buildMesh();
    
	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y );
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);
	void dragEvent(ofDragInfo dragInfo);
	void gotMessage(ofMessage msg);
	
	
	ofTrueTypeFont			font;
	ofTTFCharacter			tChar;
	char					letter;
	char					oldLetter;
	
	float					samplePixels;
	bool					bChangeSamplePixels;
	
	ofFbo					fbo;
	
	vector<ofPoint>			points;
	vector<ofxDelaunayTriangle> tris;
	vector<ofxDelaunayTriangle> origTris;
	vector<ofPoint>			centroids;
	vector<bool>			bTriRemoved;
	vector<ofPolyline>		sampledPolys;
	
	ofMesh					mesh;
	
	ofxDelaunay				delauney;
    
	ofDirectory				directory;
	int						numFonts;
	int						fontIndex;
    
    
    // shadershit
    
    //    void updateMidColor();   
    //    ofColor midColor;
    

	
};
