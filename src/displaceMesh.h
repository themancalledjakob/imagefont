#pragma once

#include "ofMain.h"

#include "makeMesh.h"

class displaceMesh : public ofBaseApp{

public:
	void setup();
    void setupCamera();
	void update();
	void draw();
	void draw2();
	
    ofImage camImage;
    
	ofVideoGrabber vidGrabber;
    
	void drawFBO(int maxHeight, bool wireframe, bool changed, ofImage * tex, int index);
    void updateMidColor();
    void updateImage(ofPixels imagePixels);
    void keyPressed(int key);
    void mouseMoved(int x, int y);
    void mousePressed(int x, int y);
    void write(string text);
        
//    ofColor midColor;
    
	ofImage colormap,bumpmap;
	ofShader shader;
	GLUquadricObj *quadratic;
    
    
    //anti aliasing
    ofFbo fbo;
    
    vector<ofFbo> fbos;
    vector<ofMesh> meshes;
    vector<ofImage> images;
    bool newLetter;
    
    ofMesh mesh;
    
    makeMesh makedMesh;
    int texWrapMethod;
    
    ofEasyCam cam;
    
    bool drawWireframe;
    bool drawDifferent;
    bool drawRotation;
    bool drawRect;
    bool drawJpeg;
    
    float meshwidths;
    
    string sayThisPlease;
    int countThisPlease;
    
    int maxHeight;
    
    float letterSpace;
        
};
