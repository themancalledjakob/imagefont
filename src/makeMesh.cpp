#include "makeMesh.h"

void makeMesh::makeTextureCoords(ofMesh * mesh, int texWrapMethod) {
    mesh->clearTexCoords();
    vector<ofVec3f> vx = mesh->getVertices();
    float step = 1.0/(float)vx.size();
    ofVec2f imgSize = ofVec2f(640, 480);
//    texWrapMethod = 3;
    bool outpotter = ofGetFrameNum() < 150;
//    if(outpotter) cout << ofGetFrameNum() << " tells you the truth" << endl;
    
    for (int i = 0; i < vx.size(); i++) {
        
        float cordX = (((float)i/(float)vx.size()));
        float cordY = i < vx.size()/2 ? .9 : .1;
        int idouble = i < vx.size()/2 ? i * 2 : (i * 2) - vx.size()/2;
        idouble = i;
        
        //        there are different methods to wrap the texture
        switch (texWrapMethod) {
            case 0:
//                mesh->addTexCoord(ofVec2f(abs((i%100-50)*0.02), abs((i%100-50)*0.02)));
                if (i <= vx.size()*1/2) { cordX = (float)i/(float)vx.size()/2; cordY = 0.0; }            
                else { cordX = 1.0-(float)(i%(vx.size()/2))/(float)vx.size()/2; cordY = 0.5; } 
                
                mesh->addTexCoord(ofVec2f( 
                                          cordX,
                                          cordY
                                          ));
                break;
                
            case 1:
                //                mesh->addTexCoord(ofVec2f(abs((i%100-50)*0.01), abs((i%100-50)*0.01)));  
                
                if (i <= vx.size()*1/8) { cordX = (float)i/(float)vx.size()/8; cordY = 1.0; }            
                else if (i <= vx.size()*2/8) { cordX = 1.0; cordY = 1.0-(float)(i%(vx.size()/8))/(float)vx.size()/8; }     
                else if (i <= vx.size()*3/8) { cordX = 1.0-(float)(i%(vx.size()/8))/(float)vx.size()/8; cordY = 0.0; }
                else if (i <= vx.size()*4/8) { cordX = 0.0; cordY = (float)(i%(vx.size()/8))/(float)vx.size()/8; } 
                
                else if (i <= vx.size()*5/8) { cordX = (float)(i%(vx.size()/8))/(float)vx.size()/8; cordY = 1.0; }            
                else if (i <= vx.size()*6/8) { cordX = 1.0; cordY = 1.0-(float)(i%(vx.size()/8))/(float)vx.size()/8; }     
                else if (i <= vx.size()*7/8) { cordX = 1.0-(float)(i%(vx.size()/8))/(float)vx.size()/8; cordY = 0.0; }
                else if (i <= vx.size()*8/8) { cordX = 0.0; cordY = (float)(i%(vx.size()/8))/(float)vx.size()/8; } 
                    
                    mesh->addTexCoord(ofVec2f( 
                                          cordX,
                                          cordY
                                          ));
                break;
                
            case 2:
                //                mesh->addTexCoord(ofVec2f(abs((i%100)*0.01), abs((i%100)*0.01)));                
                if (idouble <= vx.size()/4) cordX = (((float)(idouble%vx.size()/4)/(float)vx.size()/4));
                else if (idouble <= vx.size()/2) cordX = 1;
                else if (idouble <= 3*vx.size()/4) cordX = 1 - (((float)(idouble%vx.size()/4)/(float)vx.size()/4));
                else cordX = 0;
                
                if (idouble <= vx.size()/4) cordY = 1;
                else if (idouble <= vx.size()/2) cordY = 1 - (((float)(idouble%vx.size()/4)/(float)vx.size()/4));
                else if (idouble <= 3*vx.size()/4) cordY = 0;
                else cordY = (((float)(idouble%vx.size()/4)/(float)vx.size()/4));
                mesh->addTexCoord(ofVec2f( 
                                          cordX,
                                          cordY
                                          ));
                break;
            case 3:
//                cordX -= i < vx.size()/2 ? 0 : 0.5;
//                cordY = 1-(((float)i/(float)vx.size()));
                cordY = sin(ofGetFrameNum()*0.001f)*0.5+0.5;
                mesh->addTexCoord(ofVec2f(
                                          1.0f-cordX,
                                          cordY
                                  ));
                break;
                /*
            case 4:
                mesh->addTexCoord(ofVec2f(abs((0.2f/vx.size()*mesh->getVertex(i).x)-1.0f), mesh->getVertex(i).y*(-2)));
                break;*/
                
            case 4:
        
                mesh->addTexCoord(
                                  ofVec2f(
                                          int(abs((i*step)))%100,int(abs((i*step)))%100
                                          )
                                  );
                break;
                
            default:
                break;
        }

    }
    mesh->haveTexCoordsChanged();
    mesh->haveVertsChanged();
//    ofMesh mesh2 = *mesh;
//    return mesh2;
}

bool makeMesh::isPointInsidePolygon(ofPoint *polygon, int N, ofPoint p) {
	int counter = 0;
	int i;
	double xinters;
	ofPoint p1,p2;
	
	p1 = polygon[0];
	
	for (i=1;i<=N;i++) {
		p2 = polygon[i % N];
		if (p.y > MIN(p1.y,p2.y)) {
			if (p.y <= MAX(p1.y,p2.y)) {
				if (p.x <= MAX(p1.x,p2.x)) {
					if (p1.y != p2.y) {
						xinters = (p.y-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)+p1.x;
						if (p1.x == p2.x || p.x <= xinters)
							counter++;
					}
				}
			}
		}
		p1 = p2;
	}
	
	if (counter % 2 == 0)
		return false;
	else
		return true;
} 

bool makeMesh::isPointInsideOfShape(vector<ofVec3f> * path, int N, ofVec3f p) {
	int counter = 0;
	int i;
	double xinters;
	ofVec3f p1,p2;
	
	p1 = path[0][0];
	
	for (i=1;i<=N;i++) {
		p2 = path[0][i % N];
		if (p.y > MIN(p1.y,p2.y)) {
			if (p.y <= MAX(p1.y,p2.y)) {
				if (p.x <= MAX(p1.x,p2.x)) {
					if (p1.y != p2.y) {
						xinters = (p.y-p1.y)*(p2.x-p1.x)/(p2.y-p1.y)+p1.x;
						if (p1.x == p2.x || p.x <= xinters)
							counter++;
					}
				}
			}
		}
		p1 = p2;
	}
	
	if (counter % 2 == 0)
		return false;
	else
		return true;
} 


//--------------------------------------------------------------
void makeMesh::setup() {
    
	ofSetFrameRate(60);
	ofSetVerticalSync(true);
	ofBackground( 255, 230, 30);
	
	numFonts = directory.listDir("fonts/");
	fontIndex	= floor(3);
	//font.loadFont(directory.getPath(fontIndex), 300, true, true, true, .5);
	font.loadFont(directory.getPath(fontIndex), 300, true, true, true, .5);

	// loadFont(string filename, int fontsize, bool _bAntiAliased=true, bool _bFullCharacterSet=false, bool makeContours=false, float simplifyAmt=0.3, int dpi=0);
	
	fbo.allocate((float)ofGetWidth()*.3, (float)ofGetHeight()*.3);
	
	oldLetter	= ' ';
	letter		= 'n';
	
	samplePixels		= 33.;
	bChangeSamplePixels = true;
	
//	cout << "tris size: " << delauney.triangles.size() << endl;

}

//--------------------------------------------------------------
void makeMesh::update(ofPath path, int texWrapMethod) {
    
    ofSetWindowTitle(ofToString(ofGetFrameRate()));
    
    //    vidGrabber.update();
    //    updateMidColor();
	
	if(letter != oldLetter || bChangeSamplePixels) {
        
		tChar = font.getCharacterAsPoints(letter);
		points.clear();
		delauney.reset();
		tris.clear();
		sampledPolys.clear();
		
		centroids.clear();
		origTris.clear();
		bTriRemoved.clear();
		
		float fw = font.stringWidth( ofToString(letter) );
		float fh = font.stringHeight(ofToString(letter));
		fbo.allocate(fw, fh);
		fbo.begin();
		ofSetColor(0, 0, 0);
		ofRect(0, 0, fbo.getWidth(), fbo.getHeight());
		
		ofSetColor(255, 255, 255);
		ofRectangle bounds = font.getStringBoundingBox( ofToString(letter), 0, 0);
		font.drawStringAsShapes(ofToString(letter), -bounds.x, -bounds.y);
		fbo.end();
        
		ofPixels pix;
		fbo.readToPixels(pix);
		
		path	= font.getCharacterAsPoints(letter);
		vector <ofPolyline> polys = path.getOutline();
		// resample the font points so that we have a point every (x) pixels;
		for (int i = 0; i < polys.size(); i++) {
			
			// now resample and draw new dots
			ofPolyline sampled = polys[i].getResampledBySpacing(samplePixels);
			sampledPolys.push_back( sampled );
			// outline
			for (int j = 0; j < sampled.size(); j++) {
				points.push_back( ofVec3f(sampled[j].x, sampled[j].y, 0.));
			}
		}
        
		delauney.reset();
		for (int i = 0; i < points.size(); i++) {
			delauney.addPoint(points[i].x, points[i].y, points[i].z);
		}
		delauney.triangulate();
		
		ofPoint* srcPts = new ofPoint[points.size()];
		for (int i = 0; i < points.size(); i++) {
			srcPts[i].x = points[i].x;
			srcPts[i].y = points[i].y;
			srcPts[i].z = points[i].z;
		}
		
		ofPoint centroid;
		for( int i=0; i < delauney.triangles.size(); i++ ) {
			ofxDelaunayTriangle& triangle = delauney.triangles[ i ];
			centroid.set(0, 0, 0);
			for(int j = 0; j < 3; j++) {
				centroid += triangle.points[j];
			}
			centroid /= 3.f;
			
			origTris.push_back( ofxDelaunayTriangle() );
			origTris[origTris.size()-1].points[0] = triangle.points[0];
			origTris[origTris.size()-1].points[1] = triangle.points[1];
			origTris[origTris.size()-1].points[2] = triangle.points[2];
			
			centroids.push_back( centroid );
			
			ofColor col = pix.getColor(centroid.x-bounds.x, centroid.y-bounds.y);
			
			//cout << "centroid " << centroid.x
			
			if(col.r > 200) {
                //if(isPointInsidePolygon( srcPts, points.size(), centroid)) {
				tris.push_back( ofxDelaunayTriangle() );
				for(int t = 0; t < 3; t++) {
					tris[tris.size()-1].points[t] = triangle.points[t];
					tris[tris.size()-1].pointIndex[t] = triangle.pointIndex[t];
				}
				
				bTriRemoved.push_back( false );
			} else {
				bTriRemoved.push_back( true );
			}
		}
         
		delete srcPts;
		
		buildMesh();
        
		oldLetter = letter;
		bChangeSamplePixels = false;
	}
    
    makeTextureCoords(&mesh,texWrapMethod);
}

//--------------------------------------------------------------
void makeMesh::draw() {
    // the mesh is just loaded in maked, it will be drawn somewhere else
	
//	glEnable(GL_DEPTH_TEST);
//	glPushMatrix();
//	glTranslatef(200, 500, 0);
//    //    glRotatef(int(ofGetElapsedTimef()*100)%360, 1.0, 0.0, 0.0);
//	ofSetColor(40, 100, 130);
//	mesh.draw();
//	ofSetColor(255, 255, 255);
//	mesh.drawWireframe();
//	glPopMatrix();
//	
//	glDisable(GL_DEPTH_TEST);
    
}

//--------------------------------------------------------------
void makeMesh::buildMesh() {
	mesh.clearVertices();
	mesh.clearIndices();
	mesh.clear();
	float newz = 50.f;
	ofVec3f p1, p2, p3;
	int offsetindex = 0;
	
	// loop around base of text //
	for(int i = 0; i < sampledPolys.size(); i++) {
		for (int j = 0; j < sampledPolys[i].size(); j++) {
			p1.set(sampledPolys[i][j].x, sampledPolys[i][j].y, 0.);
			mesh.addVertex(p1);
		}
	}
	offsetindex = mesh.getNumVertices();
	// then add vertexes for the extruded text //
	for(int i = 0; i < sampledPolys.size(); i++) {
		for (int j = 0; j < sampledPolys[i].size(); j++) {
			p1.set(sampledPolys[i][j].x, sampledPolys[i][j].y, newz);
			mesh.addVertex(p1);
		}
	}
	
	ofIndexType i1, i2, i3;
	int ip	= 0;
	int ip2 = offsetindex;
	for(int i = 0; i < sampledPolys.size(); i++) {
		for (int j = 0; j < sampledPolys[i].size()-1; j++) {
			i1 = j + ip2;
			i2 = j+1 + ip2;
			i3 = j + ip;
			mesh.addTriangle(i1, i2, i3);
			i1 = j + ip;
			i2 = j+1+ip;
			i3 = j+1+ip2;
			mesh.addTriangle(i1, i2, i3);
		}
		
		i1 = ip;
		i2 = ip+sampledPolys[i].size()-1;
		i3 = ip2;
		mesh.addTriangle(i1, i2, i3);
		
		i1 = ip2;
		i2 = ip2 +sampledPolys[i].size()-1;
		i3 = ip + sampledPolys[i].size()-1;
		mesh.addTriangle(i1, i2, i3);
		
		
		ip += sampledPolys[i].size();
		ip2 += sampledPolys[i].size();
	}
	
	
	ip	= 0;
	ip2 = offsetindex;
	ofPoint tpts[3];
	bool bF[3];
	for( int i=0; i < tris.size(); i++ ) {
		ofxDelaunayTriangle& triangle = tris[ i ];// pointIndex
		tpts[0] = tris[i].points[0];
		tpts[1] = tris[i].points[1];
		tpts[2] = tris[i].points[2];
		bF[0] = bF[1] = bF[2] = false;
		for(int j = 0; j < points.size(); j++) {
			//for (int k = 0; k < sampledPolys[j].size(); k++) {
			for(int r = 0; r < 3; r++) {
				//if(sampledPolys[j][k].x == tpts[r].x && sampledPolys[j][k].y == tpts[r].y) {
				if(points[j].x == tpts[r].x && points[j].y == tpts[r].y) {
					tris[i].pointIndex[r] = j;
					bF[r]	= true;
				}
				if(bF[0] == true && bF[1] == true && bF[2] == true) break;
			}
		}
	}
//    int sideIndex = mesh.getNumIndices();
	for( int i=0; i < tris.size(); i++ ) {
		mesh.addTriangle(tris[i].pointIndex[2], tris[i].pointIndex[1], tris[i].pointIndex[0]);
		mesh.addTriangle(tris[i].pointIndex[0]+offsetindex, tris[i].pointIndex[1]+offsetindex, tris[i].pointIndex[2]+offsetindex);
	}
    
    // calculate normal
    ofVec3f normal;
   
    ofVec3f a, b, c, v1, v2, o;

    vector<int> layer1indeces;
    vector<int> layer2indeces;
    for ( int i = 0; i < mesh.getNumVertices(); i++){
        
        if (mesh.getVertex(i).z == 0) {
            layer1indeces.push_back(i);
        } else if (mesh.getVertex(i).z != 0) {
            layer2indeces.push_back(i);
        }
        
    }
    
    int layer1index = 0;
    int layer2index = 0;
    
    for ( int i = 0; i < mesh.getNumVertices(); i++){
        
        int index;
        vector<int> currentLayer;
        int z = mesh.getVertex(i).z;
        if (z == 0) {
            currentLayer = layer1indeces;
            index = layer1index;
            layer1index++;
        } else if (z != 0) {
            currentLayer = layer1indeces;
            index = layer2index;
            layer2index++;
        }    
        
        if (index == 0)
            a = mesh.getVertex(currentLayer[currentLayer.size()-1]);
        else
            a = mesh.getVertex(currentLayer[index-1]);
        
        b = mesh.getVertex(currentLayer[index]);
        
        if (index == currentLayer.size()-1)
            c = mesh.getVertex(currentLayer[0]);
        else
            c = mesh.getVertex(currentLayer[index+1]);
        
        v1 = b-a;
        v2 = b-c;
        
        if (z == 0){
            o = ofVec3f(0,0,-0.5);
        } else {
            o = ofVec3f(0,0,0.5);
        }
        
        normal.middle(v1.middle(v2));
        normal = normal.getNormalized();
        
        if ( isPointInsideOfShape(&mesh.getVertices(), mesh.getVertices().size()/2, mesh.getVertex(i)+(normal)) ) 
            normal *= -1;
        
        normal.middle(normal.middle(o));
            
        mesh.addNormal(normal*-10);
//        cout << "normal.x " << normal.x << " / normal.y :" << normal.y << " / normal.z :" << normal.z << endl;
        
    }
    
    /*
    ofVec3f a, b, c;
    a = ( tris[0].points[0] - tris[0].points[1] );
    b = ( tris[0].points[0] - tris[0].points[2] );
    
    c.x = ( a.y * b.z ) - ( a.z * b.y );
    c.y = -1 * ( ( a.x * b.z ) - ( a.z * b.x ) );
    c.z = ( a.x * b.y ) - ( a.y * b.x );
    
    normal = c*(-0.01);
     
    for ( int i = 0; i < mesh.getNumVertices(); i++){
        ofVec3f quer;
        if (i == 0)
            quer = ( mesh.getVertex(i) - mesh.getVertex(i+1) ) + ( mesh.getVertex(i) - mesh.getVertex(mesh.getNumVertices()-1) );
        if (i == mesh.getNumVertices()-1)
            quer = ( mesh.getVertex(i) - mesh.getVertex(0) ) + (mesh.getVertex(i) - mesh.getVertex(i-1));
        else
            quer = ( mesh.getVertex(i) - mesh.getVertex(i+1) ) + (mesh.getVertex(i) - mesh.getVertex(i-1));

        normal = normal.getNormalized()*5;
        quer = quer.getNormalized()*5;
//        normal = ofVec3f(0,0,0);
        
        if ( isPointInsideOfShape(&mesh.getVertices(), mesh.getVertices().size()/2, mesh.getVertex(i)+(normal+quer)) ){
            mesh.addNormal((normal+quer));
        } else {
            mesh.addNormal(-1*(normal+quer));
        }
     }
     */
    
//     cout << "mesh.getIndices().size();" << mesh.getIndices().size() << endl;
//     cout << "mesh.getVertices().size();" << mesh.getVertices().size() << endl;
//     cout << "mesh.getNormals().size();" << mesh.getNormals().size() << endl;
//     
//	
//	cout << "mesh verts= " << mesh.getNumVertices() << " indicies = " << mesh.getNumIndices() << endl;
}


//--------------------------------------------------------------
void makeMesh::keyPressed(int key) {
	cout << key << endl;
	if(key == OF_KEY_UP || key == 357) {
		samplePixels		+= .5;
		bChangeSamplePixels = true;
	} else if (key == OF_KEY_DOWN || key == 359) {
		samplePixels		-= .5;
		bChangeSamplePixels = true;
	} else if(key == OF_KEY_RIGHT) {
		fontIndex	++;
		if(fontIndex > numFonts-1) fontIndex = numFonts-1;
		font.loadFont(directory.getPath(fontIndex), 300, true, true, true, .5);
		bChangeSamplePixels = true;
	} else if (key == OF_KEY_LEFT) {
		fontIndex--;
		if(fontIndex < 0) fontIndex = 0;
		font.loadFont(directory.getPath(fontIndex), 300, true, true, true, .5);
		bChangeSamplePixels = true;
	} else if(key != ' ') {
		letter = key;
	} if(samplePixels < .01) samplePixels = 0.01;
}

//--------------------------------------------------------------
void makeMesh::keyReleased(int key) {
	
}

//--------------------------------------------------------------
void makeMesh::mouseMoved(int x, int y) {
	
}

//--------------------------------------------------------------
void makeMesh::mouseDragged(int x, int y, int button) {
	
}

//--------------------------------------------------------------
void makeMesh::mousePressed(int x, int y, int button) {
	
}

//--------------------------------------------------------------
void makeMesh::mouseReleased(int x, int y, int button){
	
}

//--------------------------------------------------------------
void makeMesh::windowResized(int w, int h) {
	
}

//--------------------------------------------------------------
void makeMesh::gotMessage(ofMessage msg) {
	
}

//--------------------------------------------------------------
void makeMesh::dragEvent(ofDragInfo dragInfo) { 
	
}

//void makeMesh::updateMidColor(){
//    if (vidGrabber.isFrameNew()){  
//        int totalPixels = 640*480;  
//        unsigned char * pixels = vidGrabber.getPixels();  
//        int totalR = 0;
//        int totalG = 0;
//        int totalB = 0;
//        
//        for (int i = 0; i < totalPixels; i++){ 
//            totalR += pixels[i*3];  
//            totalG += pixels[i*3+1];  
//            totalB += pixels[i*3+2];  
//        }  
//        // invert and put it in the midColor ofColor
//        midColor.r = 255-char((float)totalR/(float)totalPixels);
//        midColor.g = 255-char((float)totalG/(float)totalPixels);
//        midColor.b = 255-char((float)totalB/(float)totalPixels);
//        midColor.a = 255;
//    }  
//}
