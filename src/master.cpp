#include "master.h"

//--------------------------------------------------------------
void master::setup(){
    writer.setup();
    writer2.setup();
    fft.startThread();
    fft.setup();
    blur.setup(ofGetWindowWidth(), ofGetWindowHeight());
    
    woorden.push_back("A");
    
    // SECOND LINE
    
    woorden2.push_back("A");
    timer = 0;
    counter = 0;
}

//--------------------------------------------------------------
void master::update(){
//    ofPixels pix;
//    fft.fbo.getTextureReference().readToPixels(pix);
//    writer.updateImage(pix);
    writer.camImage.getTextureReference() = fft.fbo.getTextureReference();
    writer.update();
    writer2.camImage.getTextureReference() = fft.fboi.getTextureReference();
    writer2.update();
    fft.update();
    blur.setScale(fft.totalAmplitude*10);
}

//--------------------------------------------------------------
void master::draw(){
    ofTranslate(-220, -900);
    ofScale(4.0, 4.0);
    ofBackground(255);
    fft.drawInverted();
    fft.draw(); // does only draw in the fbo
    ofPushMatrix();
    ofTranslate(200, 120);
    writer.draw();
    ofPushMatrix();
    ofTranslate(0, 150);
//    writer2.draw();
    ofPopMatrix();
    ofPopMatrix();
    ofDrawBitmapString("press ctrl+... d: animate e: background r: rotation j: exportJpeg 0-4: wrapmode 5: wireframe LEFTRIGHT:font UPDOWN: resolution", 20, 20);
    
    timer++;
    int faktor = 2;
    if (timer < 10 * faktor){
        writer.drawWireframe = ofGetFrameNum()%2==0 ? true : false;
        writer2.drawWireframe = ofGetFrameNum()%3==0 ? true : false;
    } else if (timer == 10 * faktor){
        writer.drawWireframe = false;
        writer2.drawWireframe = false;
    }
    
    if (timer >= 240 * faktor){
        writer.drawWireframe = ofGetFrameNum()%2==0 ? true : false;
        writer2.drawWireframe = ofGetFrameNum()%3==0 ? true : false;
        writer.drawRotation = true;
        writer2.drawRotation = true;
    }
    if (timer >= 280 * faktor){
        writer.drawWireframe = false;
        writer2.drawWireframe = false;
        writer.drawRotation = false;
        writer2.drawRotation = false;
        
        timer = 0;
        keyPressed(OF_KEY_DEL);
        writer.write(woorden[counter]);
        writer2.write(woorden2[counter]);
        counter++;
        counter %= woorden.size();
    }
    if(ofGetKeyPressed('p')){
        ofImage temp;
        temp.grabScreen(0, 0, ofGetWindowWidth(), ofGetWindowWidth());
        temp.saveImage("blah_" + ofGetTimestampString() + ".jpg");
    }
}

//--------------------------------------------------------------
void master::keyPressed(int key){
    if (key != 'p'){
        writer.keyPressed(key);
        writer2.keyPressed(key);
    }
    fft.keyPressed(key);
}

//--------------------------------------------------------------
void master::keyReleased(int key){
    if (key != 'p'){
        writer.keyReleased(key);
        writer2.keyReleased(key);
    }
    if(key == 'f')
        ofToggleFullscreen();
}

//--------------------------------------------------------------
void master::mouseMoved(int x, int y){
//    writer.cam.setPosition(ofGetWindowWidth()-(2*x), ofGetWindowHeight()-(2*y), 0);
}

//--------------------------------------------------------------
void master::mouseDragged(int x, int y, int button){
}

//--------------------------------------------------------------
void master::mousePressed(int x, int y, int button){
    fft.mousePressed(x, y);
    writer.mousePressed(x, y);
    writer2.mousePressed(x, y);
}

//--------------------------------------------------------------
void master::mouseReleased(int x, int y, int button){
//    fft.useOutput = !fft.useOutput;
//    fft.useInput = !fft.useInput;
//    cout << "useInput " << fft.useInput << endl;
//    cout << "useOutput " << fft.useOutput << endl;
}

//--------------------------------------------------------------
void master::windowResized(int w, int h){
    
}

//--------------------------------------------------------------
void master::gotMessage(ofMessage msg){
    
}

//--------------------------------------------------------------
void master::dragEvent(ofDragInfo dragInfo){
    
}

// - ?
void master::audioIn(float *input, int bufferSize, int nChannels){
//    if(fft.useInput)
        fft.audioReceived(input, bufferSize, nChannels);
}

void master::audioOut(float *output, int bufferSize, int nChannels){
//    if(!fft.useOutput)
        fft.audioOut(output, bufferSize, nChannels);
}

void master::exit() {
    fft.stopThread();
}