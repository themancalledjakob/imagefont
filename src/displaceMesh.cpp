#include "displaceMesh.h"

GLfloat lightOnePosition[] = {40.0, 40, 100.0, 0.0};
GLfloat lightOneColor[] = {0.99, 0.99, 0.99, 1.0};

GLfloat lightTwoPosition[] = {-40.0, 40, 100.0, 0.0};
GLfloat lightTwoColor[] = {0.99, 0.99, 0.99, 1.0};

//--------------------------------------------------------------
void displaceMesh::setup(){
	ofSetVerticalSync(true);
//	ofSetWindowShape(1600, 600);
   
    camImage.allocate(640, 480, OF_IMAGE_COLOR);
    setupCamera();
         
    makedMesh.setup();
	ofBackground(0);
    
	ofSetVerticalSync(true);
    
	ofDisableArbTex();
    shader.load("shaders/displace.vert", "shaders/displace.frag");
	
	colormap.loadImage("4colorgrid.jpg");
	bumpmap.loadImage("4colors.jpg");
    
//    midColor = ofColor(0,0,0);
    
    // anti aliasing
    fbo.allocate(ofGetWidth()*2, ofGetHeight()*2);
    
    texWrapMethod = 3;
    drawWireframe = false;
    
    meshwidths = 0;
    
    drawDifferent = false;
    drawRotation = false;
    drawRect = true;
    drawJpeg = false;
    
    sayThisPlease = "the_upper_room";
    countThisPlease = 0;
    cam.setScale(2,-2,2);
    
    maxHeight = 20;
    
    letterSpace = 0;
}

void displaceMesh::setupCamera(){

}

//--------------------------------------------------------------
void displaceMesh::update(){
//  DISPLACE
    ofEnableArbTex();
    ofPath p;
    makedMesh.update(p,texWrapMethod);
    ofDisableArbTex();
    
    ofSetWindowTitle(ofToString(ofGetFrameRate()));

//    vidGrabber.update();
    
//    updateMidColor();
    
    if(newLetter) {
        fbos.push_back(fbo);
        meshes.push_back(makedMesh.mesh);
        images.push_back(camImage);
        newLetter = false;
    }
    
}

//--------------------------------------------------------------
void displaceMesh::draw(){
    ofPushMatrix();
    ofScale(0.5,0.5,0.5);
//    cout << meshes.size() << endl << sayThisPlease.size() << endl << countThisPlease << endl << endl << endl;
    if( meshes.size() < sayThisPlease.size() && countThisPlease < sayThisPlease.size() && meshes.size() == countThisPlease){
        makedMesh.keyPressed(sayThisPlease.at(countThisPlease));
        newLetter = true;
        countThisPlease++;
    }
//    ofBackground(0, 0, 0);
    if(!drawRect)
        camImage.draw(0, 0, ofGetWindowWidth(), ofGetWindowHeight());
    
    // we enable the alpha, so that the fbos don't block each other
    ofEnableBlendMode(OF_BLENDMODE_ALPHA);
        
//    ofPushMatrix();
//    ofTranslate(-180, -200);
//    ofScale(1.8, 1.8);
//    ofTranslate(-180, 0);
//    if (drawJpeg)
//        drawFBO(maxHeight, drawWireframe, true, &colormap, -1); // last bool is if it's not changed it,s not recalculated. could be useful.
//    else
//        drawFBO(maxHeight, drawWireframe, true, &camImage, -1); // last bool is if it's not changed it,s not recalculated. could be useful.
//    ofPopMatrix();
    
    meshwidths = 0;
    
    if(fbos.size() > 0) { 
    ofPushMatrix();
        for(int i = 0; i < fbos.size(); i++){
            if (drawJpeg)
                drawFBO(maxHeight, drawWireframe, true, &colormap, i);
            else if (drawDifferent)
                drawFBO(maxHeight, drawWireframe, true, &images[i], i);
            else 
                drawFBO(maxHeight, drawWireframe, true, &camImage, i);
        }
    
    ofPopMatrix();
    }
    
    ofDisableBlendMode();
    
    ofPopMatrix();
}

void displaceMesh::drawFBO(int maxHeight, bool wireframe, bool changed, ofImage * tex, int index){ //ofFbo gettexturereference
    
    if (index == -1) {
        // if it,s not changed it doesn,t have to be recalculated
        if(changed){
            fbo.begin();
            
            ofVboMesh vboMesh = makedMesh.mesh;
            
            ofBackground(0,0,0,0);
            
            shader.begin();
            shader.setUniformTexture("colormap", *tex, 1); 
            shader.setUniformTexture("bumpmap", *tex, 2);
            shader.setUniform1i("maxHeight",maxHeight);
            
            glPushMatrix();
            cam.begin();
            glEnable(GL_DEPTH_TEST);
            if (drawRotation)
                cam.orbit(((ofGetFrameNum()%360))-180., 1.0, 1000., vboMesh.getCentroid());
            ofPushStyle();
            ofSetColor(255, 0, 0);
            ofPopStyle();
            if (!wireframe) {
                vboMesh.draw();
            } else {
                ofSetColor(255, 255, 255);
                vboMesh.drawWireframe();
                for (int i = 0; i < vboMesh.getNumNormals(); i++){
                    ofPushStyle();
                    ofSetColor(255, 0, 255);
                    ofSetLineWidth(3.);
                    float random = ofRandom(30)+1.0;
                    random = 5;
                    ofLine(vboMesh.getVertex(i),vboMesh.getNormal(i)*-random+vboMesh.getVertex(i));
                    ofCircle(vboMesh.getVertex(i)+vboMesh.getNormal(i)*-random, 2);
                    ofPopStyle();
                }
                ofSetColor(255);
            }
            
            glDisable(GL_DEPTH_TEST);
            cam.end();
            glPopMatrix();
            
            shader.end();
            fbo.end();
        }
        fbo.draw(0, 0, ofGetWidth(), ofGetHeight());
    } else {
        if(changed){
            fbos[index].begin();
            
            ofVboMesh vboMesh = meshes[index];
            makedMesh.makeTextureCoords(&meshes[index],3);
            ofEasyCam cam2;
            ofBackground(0,0,0,0);
            
            shader.begin();
            shader.setUniformTexture("colormap", *tex, 1); 
            shader.setUniformTexture("bumpmap", *tex, 2);
            shader.setUniform1i("maxHeight",maxHeight);
            
            glPushMatrix();
            cam.begin();
            glEnable(GL_DEPTH_TEST);
            
            if (drawRotation)
                cam.orbit((((ofGetFrameNum()*2)%360)), 1.0, 1000., vboMesh.getCentroid());
            else
                cam.reset();

            if (!wireframe) {
                vboMesh.draw();
            } else {
                ofSetColor(255, 255, 255);
                vboMesh.drawWireframe();
                for (int i = 0; i < vboMesh.getNumNormals(); i++){
                    ofPushStyle();
                    ofSetColor(255, 0, 255);
                    ofSetLineWidth(3.);
                    float random = ofRandom(30)+1.0;
                    random = 5;
                    ofLine(vboMesh.getVertex(i),vboMesh.getNormal(i)*-random+vboMesh.getVertex(i));
                    ofCircle(vboMesh.getVertex(i)+vboMesh.getNormal(i)*-random, 2);
                    ofPopStyle();
                }
                ofSetColor(255);
            }
            
            glDisable(GL_DEPTH_TEST);
            cam.end();
            glPopMatrix();
            
            shader.end();
            fbos[index].end();
            
            
            fbos[index].draw(-700+(meshwidths)+(letterSpace*index), 80, ofGetWidth(), ofGetHeight());
            
            float smallest = 100000000000;
            float biggest = 0;
            for (int i = 0; i < meshes[index].getVertices().size(); i++){
                if (meshes[index].getVertices()[i].x < smallest)
                    smallest = meshes[index].getVertices()[i].x;
                
                if (meshes[index].getVertices()[i].x > biggest)
                    biggest = meshes[index].getVertices()[i].x;
            }
            float thiswidth = (biggest - smallest)/2 + 20;
            meshwidths += thiswidth;
        } 
    }
}

void displaceMesh::updateMidColor(){
        
//        //if (vidGrabber.isFrameNew()){
//            int totalPixels = 640*480;  
//            unsigned char * pixels = vidGrabber.getPixels(); 
//            
//            // make camImage from camera
////            camImage.setFromPixels(vidGrabber.getPixels(), 640, 480, OF_IMAGE_COLOR);
////            camImage.setUseTexture(true);
////            camImage.setImageType(OF_IMAGE_COLOR_ALPHA);
//            
//            // initialize total rgb values for computing average color
//            int totalR = 0;
//            int totalG = 0;
//            int totalB = 0;
//            
//            // compute total rgb values from pixel array
//            for (int i = 0; i < totalPixels; i++){ 
//                totalR += pixels[i*3];  
//                totalG += pixels[i*3+1];  
//                totalB += pixels[i*3+2];  
//            }  
//            
//            // invert and put it in the midColor ofColor
//            midColor.r = 255-char((float)totalR/(float)totalPixels);
//            midColor.g = 255-char((float)totalG/(float)totalPixels);
//            midColor.b = 255-char((float)totalB/(float)totalPixels);
//            midColor.a = 255;
        //}
}

void displaceMesh::updateImage(ofPixels imagePixels){
    
   // if (vidGrabber.isFrameNew()){
        int totalPixels = imagePixels.size()/4;
        unsigned char * pixels = imagePixels.getPixels();
        
        // make camImage from camera
        camImage.setFromPixels(pixels, 1600, 600, OF_IMAGE_COLOR_ALPHA);
        //camImage.setUseTexture(true);
        camImage.setImageType(OF_IMAGE_COLOR_ALPHA);
    
    
    /* job for shader */
//        // initialize total rgb values for computing average color
//        int totalR = 0;
//        int totalG = 0;
//        int totalB = 0;
//        
//        // compute total rgb values from pixel array
//        for (int i = 0; i < totalPixels; i++){
//            totalR += pixels[i*3];
//            totalG += pixels[i*3+1];
//            totalB += pixels[i*3+2];
//        }
//        
//        // invert and put it in the midColor ofColor
//        midColor.r = 255-char((float)totalR/(float)totalPixels);
//        midColor.g = 255-char((float)totalG/(float)totalPixels);
//        midColor.b = 255-char((float)totalB/(float)totalPixels);
//        midColor.a = 255;
    //}
}

void displaceMesh::keyPressed(int key){
    cout << key << endl;
    if (key == 4){ // ctrl + d
        drawDifferent =! drawDifferent;
    } else if (key == 18){ // ctrl + r
        drawRotation =! drawRotation;
    } else if (key == 20){ // ctrl + t
        meshes.clear();
        fbos.clear();
        images.clear();
        countThisPlease = 0;
        
        write("itif");
    } else if (key == 5){ // ctrl + e
        drawRect =! drawRect;
    } else if (key == 10){ // ctrl + j
        drawJpeg =! drawJpeg;
    } else if (key >= 48 && key <= 52){
        texWrapMethod = key - 48; // 0-4
    } else if (key == 53){
        drawWireframe =! drawWireframe; // 5
    } else if ( key == OF_KEY_UP || key == OF_KEY_DOWN || key == OF_KEY_LEFT || key == OF_KEY_RIGHT){
        makedMesh.keyPressed(key);
    } else if ( key == OF_KEY_DEL || key == 127 ) {
        cout << "del" << endl;
        meshes.clear();
        fbos.clear();
        images.clear();
        countThisPlease = 0;
    } else {
        makedMesh.keyPressed(key);
        newLetter = true;
    }
}

void displaceMesh::mouseMoved(int x, int y){
//    maxHeight = int(0.025 * (float)mouseY);
}

void displaceMesh::mousePressed(int x, int y){
    maxHeight = int(0.25f * (float)y);
}


void displaceMesh::write(string text) {
//    keyPressed(OF_KEY_DEL); // clear text
//    text = "nof";
//    for(countThisPlease = 0; countThisPlease < text.length(); countThisPlease++){ // remember, countThisPlease is already initiated
//        keyPressed(text.at(countThisPlease));
//        //newLetter = true;
//    }
    sayThisPlease = text;
}